const config = require("./config.json");
const { CommandoClient } = require('discord.js-commando');
const path = require('path');


// Discord connection

const client = new CommandoClient({
  commandPrefix: '!', // Préfixe des commandes (ex: ?help)
	owner: '274228088714559489', // ID de l'owner du bot, peut également etre un tableau d'id pour plusieurs owners, ex: ['ID1', 'ID2']
  disableMentions: 'everyone' // Désactive, par sécurité, l'utilisation du everyone par le bot
});

client.registry
	.registerDefaultTypes()
	.registerGroups([
		['teachers', 'Teachers\' commands'],
		['students', 'Students\' commands'],
	])
	.registerDefaultGroups()
  .registerDefaultCommands()
	.registerCommandsIn(path.join(__dirname, 'commands'));

client.once('ready', () => {
  console.log(`Connected to Discord.`);
  client.user.setActivity('IUT Nantes');
});


client.on('error', console.error);


client.login(config.BOT_TOKEN);
