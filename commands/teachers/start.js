const { Command } = require('discord.js-commando');
const teacher_dao = require('../../modules/teacher_dao.js');
const TeacherDAO = teacher_dao.TeacherDAO;

function dateFormat(date) {
	let res = date.getFullYear() + "/" +
						(date.getMonth() + 1) + "/" +
						date.getDate() + " " +
						date.getHours() + ":" +
						date.getMinutes() + ":" +
						date.getSeconds();
	return res;
}

module.exports = class Start extends Command {
	constructor(client) {
		super(client, {
			name: 'start',
			aliases: ['st'],
			group: 'teachers',
			memberName: 'start',
			description: 'Commencer le cours !',
      args: [
        {
            key: 'role',
            prompt: "Quel groupe d'étudiants ?",
            type: 'role'
        },
				{
            key: 'lessonId',
            prompt: "Quel est le nom du module ?",
            type: 'string'
        }
    ]
		});
	}

  run(message, {role, lessonId}) {

		// Checking if the sender is currently in a voice channel.
		let channel = message.member.voice.channel;

		if (!channel) {
			return message.say("Vous devez être dans un channel vocal !");
		}

		// Checking if the sender is a teacher registered in the database.
		let dao = new TeacherDAO();
		let teacher = message.member;
		let teacherId = teacher.nickname.split('-')[1].replace(' ','');

		if (!dao.isTeacher(teacherId)) {
			return message.say("Vous devez être un professeur pour lancer un cours !");
		}

		// Checking if the module is registered in the database.
		if (!dao.isLesson(lessonId)) {
			return message.say("Le module n'existe pas !");
		}

		// Getting absent and present members in the vocal channel from the group.
		var channelMembersId = channel.members.map(member => member.user.id);
		var presentStudents = [];
		var absentStudents = [];

		for (var student of role.members.values()) {
			if (channelMembersId.includes(student.user.id)) {
				presentStudents.push(student);
			} else {
				absentStudents.push(student);
			}
		}

		// Sending absence to the database
		let studentLastName, studentFirstName, studentId;
		let dateStamp = dateFormat(message.createdAt);

		for (var student of absentStudents) {
			studentLastName = student.nickname.split('-')[0].split(' ')[0];
			studentFirstName = student.nickname.split('-')[0].split(' ')[1];
			studentId = student.nickname.split('-')[1].replace(' ','');
			if (dao.isStudent(studentId)) {
				dao.addAbsence(studentId, lessonId, teacherId, dateStamp);
			}
		}

		// Display the register
		const embed = {
			"title": "Appel du cours",
			"description": "Professeur : " + teacher.nickname + "\n" +
										 "Module : " + dao.getLessonName(lessonId) + "\n" +
										 "Groupe :  " + role.toString(),
				"color": 16777215,
				"timestamp": dateStamp,
				"thumbnail": {
					"url": teacher.user.displayAvatarURL()
				},
				"fields": [
					{
						"name": ":white_check_mark: Présents",
						"value": (presentStudents.length > 0) ? presentStudents.join('\n') : "Personne",
						"inline": true
					},
					{
						"name": ":grey_question: Absents",
						"value": (absentStudents.length > 0) ? absentStudents.join('\n') : "Personne",
						"inline": true
					}
				]
			};
		message.channel.send("", {embed});
		return;
	}
};
