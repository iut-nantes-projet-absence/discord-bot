const sqliteConnection = require('./sqlite_connection.js');

class TeacherDAO {

  #database;

  constructor() {
    this.#database = sqliteConnection.getInstance().getDatabase();
  }

  addAbsence(studentId, lessonId, teacherId, dateStamp) {
    const insert = this.#database.prepare('INSERT INTO ABSENCES (student, lesson, teacher, dateStamp) VALUES (?, ?, ?, ?);');
    insert.run(studentId, lessonId, teacherId, dateStamp);
  }

  isStudent(studentId) {
    const stmt = this.#database.prepare('SELECT id FROM STUDENTS WHERE id = ?;');
    const row = stmt.get(studentId);
    return typeof row !== 'undefined';
  }

  isTeacher(teacherId) {
    const stmt = this.#database.prepare('SELECT id FROM TEACHERS WHERE id = ?;');
    const row = stmt.get(teacherId);
    return typeof row !== 'undefined';
  }

  isLesson(lessonId) {
    const stmt = this.#database.prepare('SELECT id FROM LESSONS WHERE id = ?;');
    const row = stmt.get(lessonId);
    return typeof row !== 'undefined';
  }

  getLessonName(lessonId) {
    const stmt = this.#database.prepare('SELECT name FROM LESSONS WHERE id = ?;');
    const row = stmt.get(lessonId);
    return row.name;
  }
}

module.exports = {
  TeacherDAO: TeacherDAO
}
