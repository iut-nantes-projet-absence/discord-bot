const Database = require('better-sqlite3');
const config = require("../config.json");

module.exports = class SqliteConnection {

  static #INSTANCE;
  #database;

  constructor() {
    this.#database = new Database(config.DB_PATH);
    console.log('Connected to the database.');
  }

  static getInstance() {
    if (!this.#INSTANCE) {
      this.#INSTANCE = new SqliteConnection();
    }
    return this.#INSTANCE;
  }

  getDatabase() {
    return this.#database;
  }
}
